## Development guide

[[_TOC_]]

### Docker based development and testing

For building a new image with the ruby gem:

```
$ docker build -t gcs .
```

Currently we only support trivy as a scanner.

For creating and accessing a new container based on the `gcs` image:

```
docker run --rm -it gcs bash
```

Similar to the above but mounting the source code for fast development:

```
docker run --rm -it --volume "$PWD:/home/gitlab/gcs/" gcs bash
```

When inside the container the following provides a list of commands:

```
gtcs help
```

To debug with pry:

```
cd /home/gitlab/gcs

sudo apt-get update && sudo apt-get install -y -q build-essential

sudo bundle

# add `binding.pry` to the section of code that you need to debug

bundle exec exe/gtcs scan ruby:3.0.0
```

### Environment variables inside docker container

Specify the CI/CD environment variables from the [documentation](https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-cicd-variables) either for the session with `export`, or specify them before the CLI command.

Example: Increase the debug output with the `SECURE_LOG_LEVEL` variable.

```
export SECURE_LOG_LEVEL=debug

gtcs scan ruby:3.0.0

# OR

SECURE_LOG_LEVEL=debug gtcs scan ruby:3.0.0
```

### Working with remediation

Remediation is an ee feature, that is checked with the [ee?](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/blob/d2827ba9ee933aa7b8d6dc5a319ffc39bb98959f/lib/gcs/environment.rb#L111-113) method.

Make sure to set the environment variable `GITLAB_FEATURES=container_scanning` if you are working on the remediation feature

### Running tests within docker container

From within the container:
```
cd /home/gitlab/gcs
sudo apt-get update && sudo apt-get install -y -q build-essential
sudo bundle
```

Unit tests:

```
bundle exec rake unit_test
```

Integration tests:

To run integration tests you need to specify `INTEGRATION_TEST_IMAGE` environment variable with selected Docker image.

```
export INTEGRATION_TEST_IMAGE=alpine:3.12.0
sudo ./script/setup_integration
bundle exec rake integration_test
```

Integration tests for Certificate Bundle:

```
sudo ./script/setup_integration
bundle exec rake integration_test_ca_cert
```

#### Troubleshooting

On some environments running `bundle` command can result in an `Bundler::PermissionError`. To fix it you need to run the next command in your docker container:

```
sudo chmod g+w -R /usr/local/bundle
```

### Running tests without a docker container

In case `ruby` is not installed, we recommend using `asdf` as the following:
   1. [Install `asdf`](https://asdf-vm.com/#/core-manage-asdf?id=install)
   1. Create `.tool-versions` file and add `ruby x.x.x` where `x.x.x` is the version which can be found in the [Dockerfile](../Dockerfile)
   1. Run `$ asdf install`
   1. Run `$ bundle`

Unit tests:

```
bundle exec rake unit_test
```

Integration tests:

```
bundle exec rake integration
```
